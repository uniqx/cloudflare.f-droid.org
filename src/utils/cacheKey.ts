export class CacheKey {
    url: URL;
    language: string;

    constructor(url: URL, language: string) {
        this.url = url;
        this.language = language;
    }

    toString(): string {
        return `${this.url.pathname}${this.url.search}/${this.language}`
    }
}
